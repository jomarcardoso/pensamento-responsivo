# Pensamento Responsivo

## Container e a unidade View Width

Usar a unidade de medida vw faz qualquer elemento, inclusive textos, a acompanhar o tamanho da tela.

Essa técnica consiste em separar com media-queries a exata largura em que o conteúdo fica 100% da tela, ou seja com o tamanho do container.

```
$container-width: 1180px;

.container {
	width: 100%;
	max-width: $container-width;
	margin: 0 auto;
}

.content {
	font-size: 24px; // valor fixo, pois o container não expande mais
}

@media (max-width: $container-width) {
	.content {
		font-size: 5vw; // valor relativo ao container, pois aqui o container muda sua largura conforme a tela
	}
}

```

## Seções responsivas

Cada seção ser responsável por seus pontos de quebra.

```
$mobile-width: 680px
$container-width: 1180px;

.header {
	padding: 32px;

	@media (max-width: $mobile-widt) {
		background-image: green;
	}

	@media (min-width: $mobile-width + 1) {
		background-image: red;
	}
	
	@media (min-width: $container-width + 1) {
		background-image: blue;
	}
}


```
